
import socket

enter='\r\n'

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

def Send_data(string):
    s.send(string.encode()+enter.encode())

def Receive_data():
    data = s.recv(1024)
    print(data.decode())

try:
    print('\nSending buffer')
    s.connect(('192.168.56.102', 110))
    data = s.recv(1024)
    print(data.decode())
    Send_data('USER test')
    Receive_data()
    Send_data('PASS test')
    Receive_data()
    s.close()
    print('Done!')

except:
        print('Connection Failed.')

