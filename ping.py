#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Name: Simple Ping script exercise (python3)
# Author: Garry Robertson-Lee
#
# Version: 0.1
# Description: ping's hosts in a subnet.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import os
import subprocess

subnet='192.168.1.'

for i in range(1,254):
    output=''
    ip=subnet + str(i)
    cmd='ping -n 1 ' + ip
    output = os.popen(cmd).read()
    if output.find('unreachable') > 0 :
        print(ip, 'Host down')
    else:
        print(ip,'host up')
