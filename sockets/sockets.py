#!/usr/bin/python3
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Name: sockets.py
# Author: Garry Robertson-Lee (@uk_grl)
# Decription: Basic script to fire strings at a socket for testing
# Version: 0.3
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import socket
import time
import sys

from sys import argv

def Usage():
        print('Usage: ' + scrptName + ' <Test-string> <expected-result> <Logfile-path> <ip> <port>')
        print('Space separated parameters')
        sys.exit()

def Write_log(message):
    writelog=open(log_file,'w')
    timestamp=time.strftime("%Y-%m-%d %H:%M")
    writelog.write(timestamp +' '+ message)
    writelog.close()

if len(sys.argv) < 5:
    scrptName=argv[0]
    Usage()
    sys.exit()

#~~~Over rides~~~~~~~~~~~~~~~~
test_string=argv[1]
expected_result=argv[2]
log_file=argv[3]
server = argv[4]
port = int(argv[5])
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#resolve ip address
#server_ip = socket.gethostbyname(server)

#s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
try:
    request=test_string
    s.connect((server,port))
    s.send(request.encode())
    result = s.recv(1024)

    compare=result.decode()
#Need to see if a CRLF is expected and filter if it is.
    compare=compare.rstrip('\n')

    if (compare == expected_result):
        Write_log('OK')
    else:
        Write_log('Incorrect result returned')
except:
    Write_log('Error connecting')
    exit()


