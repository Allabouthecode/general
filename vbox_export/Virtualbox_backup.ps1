﻿
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Powershell Virtual Box backup script (exports each vm to a ova file)
# Munged by: GRL_UK
# Date: 30/04/2018
# Twitter: @uk_GRL
# Version: 0.1
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#Backup location. 
$destination=("c:\management\")

#Path to executable
$vboxmanage=('c:\Program Files\Oracle\VirtualBox\VBoxManage.exe')


#Grab output of list all vm's and feed into export loop.
$OutputVariable = (& $vboxmanage list vms) 
Write-output $OutputVariable | ForEach-object {
                                                
                                                $vmname=$_.split('{')[0]
                                                $vmname = $vmname -replace'"',''
                                                $path = $destination+$vmname+'.ova' -replace' ',''
                                                write-host $vboxmanage "export" $vmname.Trim(' ') "-o" $path
                                               }
