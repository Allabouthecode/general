#!/bin/bash
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Name: vbox_simple.sh
# Munged by: Garry Robertson-Lee (Twitter: @uk_grl)
# Description: Simple Bash script to export and backup virtual box vm's
# Version: 0.1
# Date: 19/06/2018
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#Enter location of backup dir. 
#Note: Drop the final '/' as it messes with path.eg. '/path' instead of '/path/' 
bck_dir='/tmp'

#Grab all vms 
list=$(vboxmanage list vms| cut -d'{' -f1)

#Warning about where the vm's are going to be dumped.
echo '~~~~~~~~~~~~~~~~~~~~~~ Simple virtalbox export script ~~~~~~~~~~~~~~~~~'
echo Backup location is $bck_dir. 
echo Ctrl-C this if this is incorrect and update the '$bck_dir' Var in script.
echo Hit enter to continue.
echo '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
read nothing

# Export every machine
for vm in $list ; do
	echo '~~~~~~~~~~~' $vm Virtual Machine '~~~~~~~~~~~~'
	vboxmanage export $vm -o $bck_dir/$vm.ova
done
