#!/bin/bash
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Name: vbox.sh
# Description: Bash script to export and backup virtual box vm's
# Version: 0.1
# Date: 
#
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Notes: 
# 1. Get list of vms
# 2. Get location of backup dir
# 3. Chose to backup all or 1

cls
declare -i cntr

echo '1. List all VM`s' 
echo '2. Export VM`s'

read choice
#echo $choice

#echo 'Enter location of backup dir'
#read bck_dir

bck_dir='/tmp'


	list_vms() {
		list=$(vboxmanage list vms| cut -d'"' -f2)
		#echo $list
		}

	export_vms() {
		list_vms
		echo $bck_dir/$1
		read num
		cntr=0
		for v in $list ; do
		       echo $cntr.$v
		       cntr+=1
	        done
		
		echo 'Which machine number?'
		read mchn_num
	#This is not working yet
		echo $list[7]
		#Needs testing
		#vboxmange export $1 -o $bck_dir/$1.ova
		}


case "$choice" in
        1)
	    list_vms
            ;;
         
        2)
            export_vms bootable
            ;;
         
        *)
            echo $"Usage: $0 {1|2}"
            exit 1
esac


#Get machines into an array.
#Print array and choices.
#for vm in $list ; do
#	echo $vm
#	echo "${list[@]}"
#done


#Used for setup and debugging
machine='bootable'

#export_vms $machine		

#for i in $list ; do
#	echo $i
#done


