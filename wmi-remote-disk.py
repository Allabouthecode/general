#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Name: Remote free disk space script (python3)
# Author: Garry Robertson-Lee
#
# Version: 0.1
# Description: Checks for free space on remote C drive.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import wmi
import getpass

ip = input('Enter address of host (ip for now): ')
username = input('Enter username: ')
password = getpass.getpass(prompt='Enter password for user: ')
info=[]

try:
    print ('Establishing connection to', ip)
    conn = wmi.WMI(ip, user=username, password=password)
    print ('Connection established')

    for free_disk in conn.Win32_LogicalDisk():
        if (free_disk.Caption) =='C:':
            info.append(ip)
            info.append(free_disk.Caption)
            freespace=int(free_disk.FreeSpace)
            GB=round(freespace/1024/1024/1024, 2)
            info.append(GB)

except wmi.x_wmi:
       print('Connection failed to:',ip)

# This needs formatting better
print(info)
